---
title: "Administration Handbook"
linktitle: "Administration"
description: Instructions for sane DataWarehouse maintenance
---

## Accessing Django shell

Using a Python shell is the easiest way to query or modify DataWarehouse data.

The following instructions asume a working `oc` local setup. Make sure your
environment is correctly set up as explained in the [local setup] instructions.

### Read Only instance **[Recommended]**

If no modifications to production data are necessary, the safest way to interact
with the database is to spin up the [datawarehouse-webservice-readonly]
application.

By default, the `datawarehouse-webservice-readonly` deployment config is set to
0 replicas. In order to be available it's necessary to scale it up first:

```bash
oc --context mpp_prod_external scale dc/datawarehouse-webservice-readonly --replicas=1
```

After a few seconds, the pod will have started and be ready to accept
connections. Use the following command to access the Python shell:

```bash
oc --context mpp_prod_external rsh dc/datawarehouse-webservice-readonly python3 manage.py shell_plus
```

When the application is not necessary anymore, scale it down back to 0 replicas
in order to save resources on the namespace:

```bash
oc --context mpp_prod_external scale dc/datawarehouse-webservice-readonly --replicas=0
```

### Write-enabled instance

If required, it's possible to open a shell on the production pods, where the
database credentials allow writing to the database.

As this step is executed on already running pods, no scale up/down is
necessary.

**Warning:** Executing commands on production pods affects production data,
make sure you know what you are doing 🙈

```bash
oc --context mpp_prod_external rsh dc/datawarehouse-webservice python3 manage.py shell_plus
```

[local setup]: https://gitlab.cee.redhat.com/cki-project/deployment-all/#local-setup
[datawarehouse-webservice-readonly]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/tree/main/openshift/datawarehouse-webservice-readonly
