---
title: Fixing missing OSCI results
linkTitle: Missing OSCI results
description: How to investigate a report of a missing OSCI results
---

## Problem

You get a report of missing OSCI results, e.g.

```plain
Any idea why CKI results don't appear on OSCI dashboard?
https://dashboard.osci.redhat.com/#/artifact/brew-build/aid/35242921
```

## Steps

1. From the OSCI dashboard, determine the Brew `Task ID`.

1. Check the pipeline-trigger logs in Grafana via [`{deployment="pipeline-trigger"} |=
   "Task ID"`][pipeline-trigger] for messages related to the `Task ID`.
   Change the filter expression to a suitable part of the time stamp to see the
   context of the messages and whether a pipeline was actually triggered and or
   any errors occurred. Determine the `Pipeline URL`.

1. Check the pipeline URL obtained from the logs for any problems. Select any
   of the `createrepo` jobs, and visit the DataWarehouse link at the end of the
   logs. From there, determine the `Checkout ID`.

1. Check the gating reporter logs in Grafana via [`{deployment="gating-reporter"}
   |= "Checkout ID"`][gating-reporter] for messages related to the `Checkout ID`.
   If there are any error messages, continue with [investigating UMB problems].

1. Check that Datagrepper successfully received all messages related to the
   `Task ID` on the [ci.cki.brew-build.test] topics. Change the `contains`
   query string parameter to match the `Task ID`. If there are missing
   messages, continue with [investigating UMB problems].

1. In the case that UMB messages were not sent because of gating-reporter
   configuration problems, DataWarehouse can be made to resend the RabbitMQ
   messages that in turn trigger gating-reporter to send the UMB messenges. [Log
   into the DataWarehouse production instance][dw-prod], and reset the
   `ready_to_report` field for the appropriate rows via something like

   ```python
   KCIDBCheckout.objects.filter(id__in=(
      'redhat:brew-12345678',
      'redhat:brew-23456789',
   ).update(ready_to_report=False)
   ```

   Changes should be picked up within 10 minutes by the [ReadyToReportCheckouts
   cron job][dw-cron].

[pipeline-trigger]: https://applecrumble.internal.cki-project.org/explore?orgId=1&left=%5B%22now-1d%22,%22now%22,%22Loki%22,%7B%22expr%22:%22%7Bdeployment%3D%5C%22pipeline-trigger%5C%22%7D%7C%3D%5C%2212345%5C%22%22%7D%5D
[gating-reporter]: https://applecrumble.internal.cki-project.org/explore?orgId=1&left=%5B%22now-1d%22,%22now%22,%22Loki%22,%7B%22expr%22:%22%7Bdeployment%3D%5C%22gating-reporter%5C%22%7D%20%7C%3D%20%5C%2212345%5C%22%22%7D%5D
[investigating UMB problems]: umb-problems.md
[ci.cki.brew-build.test]: https://datagrepper.engineering.redhat.com/raw?topic=/topic/VirtualTopic.eng.ci.cki.brew-build.test.complete&topic=/topic/VirtualTopic.eng.ci.cki.brew-build.test.error&contains=12345
[dw-prod]: ../datawarehouse/handbook.md
[dw-cron]: https://gitlab.com/cki-project/datawarehouse/-/blob/main/datawarehouse/cron/jobs.py
